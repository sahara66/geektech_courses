# Напишите функцию которая принемает *args
social_networks = [
    {
        'name': 'Facebook.com',
        'foundation_date': 2004,
        'country_creation': 'USA'
    },
    {
        'name': 'vkontakte.ru',
        'foundation_date': 2006,
        'coutry_creation': 'Russia'
    },
    {
        'name': 'twitter',
        'foundation_date': 2006,
        'coutry_creation': 'USA'
    }
]

def print_something(something):
    print('funkcia chto to vivodit', something)

def print_with_args(*args):
    print('*****function with args*****', args, sep='\n')

print_with_args(social_networks)

# Напишите функцию которая принемает *kwargs

def vyzov(**kwargs):
    print(kwargs)

vyzov(first='я не знаю что это', vyzov='Отличный урок!')
print()


# Напишите функцию которая принемает *args и проходит циклом по пришедшмим данным
def prohodit(*args):
    s = 5
    for something in args:
        s *= something
    print(s)
prohodit(4)
prohodit(1, 2, 3)
print()



# Напишите имитацию работу клуба (массив девушек, массив парней, охраник, администратор), но с использованием параметров args, kwargs
girls = [
    {
        'name': 'Zhanna',
        'age': 17
    },
    {
        'name': 'Marina',
        'age': 21
    }
]

men = [
    {
        'name': 'Ruslan',
        'age': 16
    },
    {
        'name': 'Temirlan',
        'age': 20
    }
]

def club(*args, **kwargs):
    for i in args:
        for people in i:
            if people['age'] >= 18:
                print(f"{kwargs['security']}: {people['name']} проходите!")
                print(f"{kwargs['administrator']}: Поставил печать {people['name']}\n")
            else:
                print(f"{kwargs['security']}: {people['name']} не проходите!\n")


club(girls, men, administrator="Malik", security="GBR")


