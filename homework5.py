# Создайте массив состоящий из словарей с данными
country = [
    {
        'name': 'Kyrgyzstan',
        'capital': 'Bishkek',
        'population': 6500000
    },

    {
        'name': 'USA',
        'capital': 'Washington',
        'population': 320000000
    },
    {
        "name": "Mexico",
        'capital': "Capital",
        "population": 1200000000
    }
]
# Напишите функцию которая принимает ранее созданный массив, фильтрирует
# полученный массив и возвращающий не менне двух элементов из массива
def filter(arr):
    newMass = []

    for i in arr:
        if i["population"] >= 7000000:
            newMass.append(i)

    return newMass


filteredMass = filter(country)
# измененные данные с добавленными значениями

def addKey(arr):
    for i in arr:
        i["materic"] = "America"

    return arr

addedKeys = addKey(filteredMass)

# Напишите функцию которая принимает массив ранее измененых данных,
# меняет значение в каждом из элементов и возращает измененные данные
def changeKeys(arr):
    for i in arr:
        i["materic"] = "Asia"

    return arr


changedMass = changeKeys(addedKeys)
# Напишите функцию которая принимает массив ранее измененых данных,
# и поочередно выводит их в консоль
def vyvod(arr):
    for i in arr:
        print(i)

vyvod(changedMass)
