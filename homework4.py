# Написать функцию которая принимает числа, выводит сумму чисел. Функцию надо вызвать.
a = (9)
b = (8)
c = (a + b)


def num(parametr):
    print(parametr)
for i in a, b, c:
    num(i)
# Написать функцию которая принимает числа, выводит разность чисел. Функцию надо вызвать.
a = (9)
b = (8)
c = (a - b)


def num(parametr):
    print(parametr)
for i in a, b, c:
    num(i)
# Написать функцию которая принимает числа, выводит произведение чисел. Функцию надо вызвать.
a = (9)
b = (8)
c = (a * b)


def num(parametr):
    print(parametr)
for i in a, b, c:
    num(i)

# Написать функцию которая принимает числа, выводит деление чисел. Функцию надо вызвать.
a = (9)
b = (8)
c = (a / b)


def num(parametr):
    print(parametr)
for i in a, b, c:
    num(i)
# Написать функцию которая принемает массив, проходится по циклом по массиву и печатает объекты массива. Функцию надо вызвать.
models = ['audi', 'volkswagen', 'toyota']

def cars(parametr):
    print(parametr)

for i in models:
    cars(i)


# Напишите приммеры использования всех операций с массивами
    # len()
massiv  = ['aeroflot', 'uralairlines', 'flydubai']

def aviaCompany(parametr):
    print(len(massiv))
for i in massiv:
    aviaCompany(i)

    # append()
podruga_1=['aizhana']
podruga_1.append('Zhibek')
print(podruga_1)

    # clear()
list = [{1, 2}, ('a'), ['1.1', '2.2']]
list.clear()
print('List:', list)


    # count()
website_list = ['google.com','namba.kg', 'yandex.ru', 'google.com']
count= website_list.count('google.com')
print('google.com found', count)

    # copy()
website_list = ['google.com','namba.kg', 'yandex.ru']
new_list = website_list.copy()
print('Copied List',new_list)
    # extend()
website_list = ['google.com', 'namba.kg', 'yandex.ru']
website_list_2 =['yahoo.com', 'fb.com']
website_list.extend(website_list_2)
print('Newer Website List:', website_list)
    # index()
bukvy=['a', 'b', 'c', 'd', 'e']
index=bukvy.index('c')
print('The index of c:', index)
index=bukvy.index('e')
print('The index of e:', index)
    # remove('Meder')
friends=['Sultan', 'Dastan', 'Meder']
friends.remove('Meder')
print('Updated Friend List', friends)

    # reverse()

systems = ['Windows', 'macOS', 'Linux']
print('Original List:', systems)
systems.reverse()
print('Updated List:', systems)
    # pop()
countrys = ['Australia', 'Kyrgyzstan', 'USA', 'China', 'Korea']
return_value = countrys.pop(3)
print('Return Value:', return_value)
print('Updated List:', countrys)

# Оберните все операции в функции, которые принимают масссив и выполняют над нимм операцию. Функцию надо вызвать.
    # len()
massiv=['aeroflot', 'uralairlines', 'flydubai']
def aviaCompany(parametr):
    print(len(massiv))
for i in massiv:
	aviaCompany(i)

    # append()
cats=['Leopard', 'Lion', 'Pantera', 'Tiger']
def Append(parametr):
    parametr.append('Bars')
    print(parametr)
Append(cats)

    # clear()
cats=['Leopard', 'Lion', 'Pantera', 'Tiger']
def Clear(parametr):
    parametr.clear()
    print(parametr)
Clear(cats)

    # count()
cats=['Leopard', 'Lion', 'Pantera', 'Tiger']
def Count(parametr):
    a = parametr.count('Lion')
    print(a)
Count(cats)
    # copy()
cars = ['Mazda', 'Hyundai', 'Kia']
def copiedauto(Cars):
    a = cars.copy()
    print(a)
copiedauto(cars)

    # extend()
cars = ['Mazda', 'Hyundai', 'Kia']
def carsMod(parametr):
    cars = ('Ford', 'Honda')
    parametr.extend(cars)
    print(parametr)
carsMod(cars)


    # index()
cats=['Leopard', 'Lion', 'Pantera', 'Tiger']
def Index(parametr):
    a = parametr.index('Lion')
    print(a)
Index(cats)

    # remove()
num = [1, 2, 3, 4, 5, 6, 7, 8, 9]
def Remove(parametr):
    parametr.remove(5)
    print(parametr)
Remove(num)

    # reverse()
num = [1, 2, 3, 4, 5, 6, 7, 8, 9]
def Reverse(parametr):
    parametr.reverse()
    print(parametr)
Reverse(num)
    # pop()
num = [1, 2, 3, 4, 5, 6, 7, 8, 9]
def Pop(parametr):
    parametr.pop(0)
    print(parametr)
Pop(num)
